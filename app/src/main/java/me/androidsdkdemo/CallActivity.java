package me.androidsdkdemo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import me.clique.sdk.ApiClient;
import me.clique.sdk.CallListener;
import me.clique.sdk.CallState;
import me.clique.sdk.Clique;
import me.clique.sdk.Conference;
import me.clique.sdk.ConferenceEventsListener;
import me.clique.sdk.ConferenceMember;
import me.clique.sdk.WebServiceListener;
import me.cliquedialingdemo.R;

public class CallActivity extends AppCompatActivity {


    Conference conference = null;
    ToggleButton btnMute;
    ToggleButton btnLoudspeaker;
    FloatingActionButton btnStartCall;
    FloatingActionButton btnEndCall;
    Button btnInvite;
    View panelCall;
    TextView txtStatus;

    //If empty then create new conference
    public final static String CONFERENCE_ID = "";

    //call with user and conference create

    private void conferenceCall(){
        updateCallStatus(getString(R.string.connecting));

        final ApiClient apiClient = Clique.apiClient();
        apiClient.createNewUser("Sample user", new WebServiceListener<ConferenceMember>() {
            @Override
            public void onResponse(final ConferenceMember member) {

                WebServiceListener<Conference> conferenceServiceListener = new WebServiceListener<Conference>(){
                    @Override
                    public void onResponse(Conference conference) {
                        CallActivity.this.conference = conference;
                        conference.call(member, callListener, conferenceEventsListener);
                        updateUI();
                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                    }

                };

                if(TextUtils.isEmpty(CONFERENCE_ID)){
                    apiClient.createConferenceWithModerator(member.getUuid(), conferenceServiceListener);
                }
                else{
                    apiClient.getConferenceWithId(CONFERENCE_ID, conferenceServiceListener);
                }
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }
        });
    }

    //UI

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);


        btnMute = (ToggleButton) findViewById(R.id.btn_mute);
        btnLoudspeaker = (ToggleButton) findViewById(R.id.btn_loudspeaker);
        btnStartCall = (FloatingActionButton) findViewById(R.id.btn_call_start);
        btnEndCall = (FloatingActionButton) findViewById(R.id.btn_call_end);
        panelCall = findViewById(R.id.panel_call);
        txtStatus = (TextView) findViewById(R.id.txt_call_status);
        btnInvite = (Button) findViewById(R.id.btn_invite);

    }

    public void onCallStartClick(View view){
        checkPermissonsAndCall();
    }

    public void onCallEndClick(View view){
        if(conference != null)
            conference.hangup();
    }

    public void onMutePressed(View view){
        if(conference != null) {
            if (conference.isMuted()) {
                conference.unmute();
            } else {
                conference.mute();
            }
        }
    }

    public void onLoudSpeakerPressed(View view){
        if(conference != null){
            conference.setSpeakerOutput(!conference.isSpeakerOutput());
        }
    }

    //invite to conference with url
    public void onInviteBtnClick(View view) {
            if(conference == null)
                return;

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");

            String subject = "Conference id";
            String conferenceId = conference.getId();

            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT, conferenceId);
            startActivity(intent);
        }




    private void updateUI(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                panelCall.setVisibility(conference == null ? View.GONE : View.VISIBLE);
                btnEndCall.setVisibility(conference == null ? View.GONE : View.VISIBLE);
                btnStartCall.setVisibility(conference == null ? View.VISIBLE : View.GONE);
                if(conference != null){
                    boolean isCalling = conference.getCallState() == CallState.Active;
                    btnMute.setEnabled(isCalling);
                    btnLoudspeaker.setEnabled(isCalling);
                    btnInvite.setEnabled(isCalling);
                    btnMute.setChecked(conference.isMuted());
                    btnLoudspeaker.setChecked(conference.isSpeakerOutput());

                }
            }
        });
    }

    private void updateCallStatus(String callStatus){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtStatus.setText(callStatus);
            }
        });
    }



    //Ask and check permissions for latest android versions
    static final int PERMISSION_GRANT_ID = 1;

    public boolean checkPermissonsAndCall() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.USE_SIP, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE},
                PERMISSION_GRANT_ID);
        return true;
    }

    //Dialing phone number feature
    /**
    public void dialPhoneNumber(){
        DialPhoneProvider dialPhoneProvider = new DialPhoneProvider();
        DialPhoneProvider.Options options = dialPhoneProvider.getOptions();
//      the phone number that will be shown to a Callee.
        options.setSource("11234567890");
//      enable voicemail beep detection.
        options.setDetectBeep(false);
//      enable live call detection.
        options.setDetectLive(false);
        dialPhoneProvider.call("+14156716115", new CallListener() {
            @Override
            public void onCallStarting() {

            }

            @Override
            public void onCallEnd() {

            }



            @Override
            public void onCallEstablished() {

            }

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onWaitModeratorBegin() {

            }

            @Override
            public void onWaitModeratorEnd() {

            }

            @Override
            public void onWatchMode() {

            }

            @Override
            public void onNetworkQualityChanged(boolean isNetworkQualityBad) {

            }
        });
    }
     */


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_GRANT_ID: {
                boolean isGranted = true;
                for (int i = 0; i < grantResults.length; i++)
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        isGranted = false;
                if (isGranted) {
                    conferenceCall();
                }
                return;
            }
        }
    }

    //event handlers

    CallListener callListener = new CallListener() {
        @Override
        public void onCallStarting() {
            updateUI();

        }

        @Override
        public void onCallEnd() {
            updateCallStatus(getString(R.string.disconnected));
            CallActivity.this.conference.close();
            CallActivity.this.conference = null;
            updateUI();

        }

        @Override
        public void onTimedOutPerformingConnection() {

        }

        @Override
        public void onCallEstablished() {
            updateCallStatus(getString(R.string.connected));
            updateUI();
        }

        @Override
        public void onError(Exception e) {
            updateCallStatus(getString(R.string.error) + e.getMessage());
            updateUI();
        }

        @Override
        public void onWaitModeratorBegin() {

        }

        @Override
        public void onWaitModeratorEnd() {

        }

        @Override
        public void onWatchMode() {

        }

        @Override
        public void onNetworkQualityChanged(boolean isNetworkQualityBad) {
            updateCallStatus(isNetworkQualityBad ? getString(R.string.connection_quality_bad) : getString(R.string.connected));

        }
    };
    ConferenceEventsListener conferenceEventsListener = new ConferenceEventsListener() {
        @Override
        public void onAddingMember(ConferenceMember user) {

        }

        @Override
        public void onDeletingMember(ConferenceMember user) {

        }

        @Override
        public void onStartTalking(ConferenceMember user) {

        }

        @Override
        public void onStopTalking(ConferenceMember user) {

        }

        @Override
        public void onMutingMember(ConferenceMember user) {

        }

        @Override
        public void onUnmutingMember(ConferenceMember user) {

        }

        @Override
        public void onConferenceLock() {

        }

        @Override
        public void onUnlockConference() {

        }

        @Override
        public void onKickingMember(ConferenceMember user) {

        }

        @Override
        public void onConferenceCreate() {

        }

        @Override
        public void onConferenceDestroy() {

        }

        @Override
        public void onCallStatusChanged(ConferenceMember user) {

        }

        @Override
        public void onStartRecording() {

        }

        @Override
        public void onStopRecording() {

        }

        @Override
        public void onScreenshareStart(ConferenceMember user) {

        }

        @Override
        public void onScreenshareStop() {

        }
    };



}
