package me.androidsdkdemo;

import android.app.Application;
import android.text.TextUtils;

import org.junit.Assert;

import me.clique.sdk.Clique;

/**
 * Created by SWW on 24.11.2017.
 */


public class App extends Application {

    //!WARNING: Paste API_KEY before running the app
    private static final String API_KEY = "";

    @Override
    public void onCreate() {
        super.onCreate();
        Assert.assertFalse("Fill API_KEY before running the app", TextUtils.isEmpty(API_KEY));

        Clique.init(getApplicationContext(), API_KEY);
    }
}